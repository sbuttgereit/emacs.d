;;;; BBDB and BBDB-ext
;; Now at version 3
(use-package  bbdb
  :ensure t
  :config
  (progn
    (setq bbdb-file "~/emacsprojects/.bbdb")
    ;; control pop-up and it's size
    (setq bbdb-message-all-addresses t)
    (add-hook 'message-setup-hook 'bbdb-mail-aliases)
    ))
(use-package  bbdb-ext
  :ensure t
  :defer t)
